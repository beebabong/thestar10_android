package com.gmm.thestar10.vote;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.gmm.thestar10.R;
import com.gmm.thestar10.menu_Adapter;
import com.gmm.thestar10.flyoutmenu.FlyOutContainer;
import com.gmm.thestar10.vote.Vote_Adapter;

public class Vote_Activity extends Activity implements OnClickListener {
	
	// menu
		FlyOutContainer root;
		ListView listview_main_menu;
		ImageView btn_menu;
		//Activity in this
		ImageView btn_howtovote;
		GridView gridView_vote;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.ac_vote, null);
		setmenu();
		mapping();
		setaction();
		
	
		this.setContentView(root);
	}
	
	private void setaction() {
		btn_menu.setOnClickListener(this);
		btn_howtovote.setOnClickListener(this);
		
		gridView_vote.setAdapter(new Vote_Adapter(this));
	}


	private void mapping() {
		btn_menu = (ImageView) root.findViewById(R.id.btn_menu);
		btn_howtovote = (ImageView) root.findViewById(R.id.btn_howtovote);
		gridView_vote = (GridView) root.findViewById(R.id.gridView_vote);
	}


	private void setmenu() {
		listview_main_menu = (ListView) root.findViewById(R.id.listview_main_menu);
		listview_main_menu.setAdapter(new menu_Adapter(this,0));
		
		listview_main_menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long arg3) {
				Log.i("", "click :"+pos);
//				if(pos==0)
//				else if(pos==1)
//				else if(pos==2)
//				else if(pos==3)
//				else if(pos==4)
//				else if(pos==6)
//				else if(pos==7)
//				else if(pos==9)
//				else if(pos==10)
//				else if(pos==11)
					
			}
		});
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_menu:
			root. toggleMenu();
			
			break;

		default:
			break;
		}
		
	}
	
	
}
