package com.gmm.thestar10.vote;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gmm.thestar10.R;

public class Vote_Adapter extends ArrayAdapter<String> {
	  Context context;
	  String[] text_list = {"No.1 ","No.2 ","No.3 ","No.4 ","No.5 ","No.6 ","No.7 ","No.8 "};
	  
	  static class ViewHolder {
		 static ImageView img_profile,btn_vote;
		 static TextView txt_name;
		}
	  
	  public Vote_Adapter(Context context) {
		  super(context, R.layout.list_in_profile);
		  this.context = context;
	  }
	  
	  @Override
	  public int getCount() {
		return 8;
	  }
	  
	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView;
	    
	    	 rowView = inflater.inflate(R.layout.list_in_profile, parent, false);
	    	 ViewHolder.img_profile = (ImageView) rowView.findViewById(R.id.img_profile);
	    	 ViewHolder.btn_vote = (ImageView) rowView.findViewById(R.id.btn_vote);
	    	 ViewHolder.txt_name = (TextView) rowView.findViewById(R.id.txt_name);
	    	 
	    	 
	    	 ViewHolder.txt_name.setText(text_list[position]) ;
	   
	    
	    return rowView;
	  }
	  
	  
	} 

