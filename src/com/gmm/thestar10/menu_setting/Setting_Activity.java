package com.gmm.thestar10.menu_setting;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;

import com.gmm.thestar10.R;
import com.gmm.thestar10.menu_Adapter;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.flyoutmenu.FlyOutContainer;
import com.gmm.thestar10.menu_gallery.Gallery_Activity;
import com.gmm.thestar10.menu_news.News_Activity;
import com.gmm.thestar10.menu_video.Video_Activity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class Setting_Activity extends Activity implements OnClickListener,OnTouchListener{
	// menu
			FlyOutContainer root;
			ListView listview_main_menu;
			ImageView btn_menu;
	
	//Activity in this
			ImageView btn_howtovote,img_profile,img_login,icon_howto,img_noti;
			TextView txt_name_fb,txt_number_phone;
			LinearLayout layout_main;
			
	//Other
		    DisplayImageOptions options;
			protected ImageLoader imageLoader = ImageLoader.getInstance();
			
			@Override
			protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.ac_setting, null);
				this.setContentView(root);
				
				 options = new DisplayImageOptions.Builder()
					.showStubImage(R.drawable.img_profile)
					.showImageForEmptyUri(R.drawable.img_profile)
					.showImageOnFail(R.drawable.img_profile).cacheInMemory()
					.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
			     imageLoader.init(ImageLoaderConfiguration.createDefault(Setting_Activity.this));

			        
				setmenu();
				mapping();
				setAction();
			}
	
	
	private void setAction() {
//		layout_shownews.setOnTouchListener(this);
		layout_main.setOnTouchListener(this);
	}
	
	private void mapping() {
		
		btn_howtovote = (ImageView) root.findViewById(R.id.btn_howtovote);
		img_profile = (ImageView) root.findViewById(R.id.img_profile);
		img_login = (ImageView) root.findViewById(R.id.img_login);
		icon_howto = (ImageView) root.findViewById(R.id.icon_howto);
		img_noti = (ImageView) root.findViewById(R.id.img_noti);
		txt_name_fb = (TextView) root.findViewById(R.id.txt_name_fb);
		txt_number_phone = (TextView) root.findViewById(R.id.txt_number_phone);
		layout_main = (LinearLayout) root.findViewById(R.id.layout_main);
	}
	
	//******************************************************************************************************************************************************
		//********************************************************************SET MENU**************************************************************************
		//******************************************************************************************************************************************************
		
		private void setmenu() {
			listview_main_menu = (ListView) root.findViewById(R.id.listview_main_menu);
			btn_menu = (ImageView) root.findViewById(R.id.btn_menu);
			
			listview_main_menu.setAdapter(new menu_Adapter(this,0));

			btn_menu.setOnClickListener(this);
			listview_main_menu.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View v, int pos,
						long arg3) {
					Log.i("click :"+pos);
					 
					String ActivityTarget = null;
			        Class<?> classTarget = null;
			        
			        switch (pos) {
			        case 0: ActivityTarget="News_Activity"; classTarget = News_Activity.class; break;
					case 1: ActivityTarget="Video_Activity"; classTarget = Video_Activity.class; break;
					case 2: ActivityTarget="Gallery_Activity"; classTarget = Gallery_Activity.class; break;
			        
			        }
					 
			        insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity(ActivityTarget,new Intent(Setting_Activity.this,classTarget).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
					
				}
			});
		}

		private void insertDecorView(View view) {
			getParent().setContentView(
					view,
					new LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT));
		}
	
	
		//******************************************************************************************************************************************************
		//************************************************************ SET CLICK / TOUCH ***********************************************************************
		//******************************************************************************************************************************************************

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_menu:
				root. toggleMenu();
				break;
			case R.id.btn_howtovote:
				break;
			case R.id.img_profile:
				break;
			case R.id.img_login:
				break;
			case R.id.icon_howto:
				break;
			case R.id.img_noti:
				break;
			}
			
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			int MIN_DISTANCE = 200;
			float downX = 0, downY = 0, upX, upY;
			
			 switch(event.getAction()){
		        case MotionEvent.ACTION_DOWN: {
		            downX = event.getX();
		            downY = event.getY();
		            return true;
		        }
		        case MotionEvent.ACTION_UP: {
		            upX = event.getX();
		            upY = event.getY();

		            float deltaX = downX - upX;
		            float deltaY = downY - upY;

		            // swipe horizontal?
		            if(Math.abs(deltaX) > MIN_DISTANCE){
		                // left or right
		                if(deltaX < 0 || deltaX > 0) { root. toggleMenu(); }
		            }
		           
		            return true;
		        }
		    }
		    return false;
		}

}
