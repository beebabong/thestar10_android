package com.gmm.thestar10.menu_news;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.gmm.thestar10.R;
import com.gmm.thestar10.ShareControl.ShareHelper;
import com.gmm.thestar10.Untilitys.Constants;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class Show_Gallery_Activity extends Activity implements OnClickListener{

	Bitmap bitmapImg ;
	ImageView img_prev,img_next,img_share,img_save;
	ImageView big_thumb;
	public static List<String> url_Gal = new ArrayList<String>();
	public static int position = 0 ;
	 DisplayImageOptions options;
	 protected ImageLoader imageLoader = ImageLoader.getInstance();
	 private ProgressDialog mProgressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_gallerry);
		
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.img_emply)
		.showImageForEmptyUri(R.drawable.img_profile)
		.showImageOnFail(R.drawable.img_profile).cacheInMemory()
		.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        
		 mapping();
		 setAction();
	}
	private void mapping() {
		big_thumb = (ImageView) findViewById(R.id.big_thumb);
		img_prev = (ImageView) findViewById(R.id.img_prev);
		img_next = (ImageView) findViewById(R.id.img_next);
		img_share = (ImageView) findViewById(R.id.img_share);
		img_save = (ImageView) findViewById(R.id.img_save);
	
	}
	
	private void setAction() {

		 imageLoader.displayImage(url_Gal.get(position), big_thumb, options);
		 
		img_prev.setOnClickListener(this);
		img_next.setOnClickListener(this);
		img_share.setOnClickListener(this);
		img_save.setOnClickListener(this);
	
	}
	


	//******************************************************************************************************************************************************
	//********************************************************************SET CLICK ************************************************************************
	//******************************************************************************************************************************************************
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_prev:
			position--;
			Log.i("position : "+position+"//"+url_Gal.size());
			if(position<0) position = url_Gal.size()-1;
			imageLoader.displayImage(url_Gal.get(position), big_thumb, options);
			
			break;
			
		case R.id.img_next:
			position++;
			Log.i("position : "+position+"//"+url_Gal.size());
			if(position>=url_Gal.size()) position = 0;
			imageLoader.displayImage(url_Gal.get(position), big_thumb, options);
			break;
			
		case R.id.img_share:
			String URLForShare = "";
			String word = Unsteady.news.get(Photo_Of_News_Activity.position).getCOL_title();	
			new ShareHelper(Show_Gallery_Activity.this,word,Unsteady.news.get(position).getCOL_cover(),URLForShare);
			break;
	
		case R.id.img_save:
			mProgressDialog = new ProgressDialog(Show_Gallery_Activity.this);
			mProgressDialog.setMessage("Loading ..");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.THEME_HOLO_LIGHT);
			mProgressDialog.setCancelable(true);

			
			final DownloadTask downloadTask = new DownloadTask(Show_Gallery_Activity.this);
			downloadTask.execute(url_Gal.get(position));

			mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			    @Override
			    public void onCancel(DialogInterface dialog) {
			        downloadTask.cancel(true);
			    }
			});
			break;

		
		}
		
	}
	

	//******************************************************************************************************************************************************
	//********************************************************************SAVE IMAGE************************************************************************
	//******************************************************************************************************************************************************
	
	private class DownloadTask extends AsyncTask<String, Integer, String> {

	    private Context context;

	    public DownloadTask(Context context) {
	        this.context = context;
	    }

	    @Override
	    protected String doInBackground(String... sUrl) {
	        // take CPU lock to prevent CPU from going off if the user 
	        // presses the power button during download
	        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	             getClass().getName());
	        wl.acquire();

	        try {
	            InputStream input = null;
	            OutputStream output = null;
	            HttpURLConnection connection = null;
	            try {
	                URL url = new URL(sUrl[0]);
	                connection = (HttpURLConnection) url.openConnection();
	                connection.connect();
	                
	                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
	                     return "Server returned HTTP " + connection.getResponseCode() 
	                         + " " + connection.getResponseMessage();

	                int fileLength = connection.getContentLength();

	                // download the file
	                input = connection.getInputStream();
	                File root = android.os.Environment.getExternalStorageDirectory(); 
	                String pathFile = root.getAbsolutePath()+Constants.Path_SDCardRoot_image ;
	                Log.i("path file : "+pathFile);
	                
	                if(!new File (pathFile).exists()) new File (pathFile).mkdirs();
	                		           
	                output = new FileOutputStream(pathFile+Unsteady.news.get(Photo_Of_News_Activity.position).getCOL_newsGallerys().get(position).getCOL_ID()+
	                		".png");
	                

	                byte data[] = new byte[4096];
	                long total = 0;
	                int count;
	                while ((count = input.read(data)) != -1) {
	                    // allow canceling with back button
	                    if (isCancelled())
	                        return null;
	                    total += count;
	                    // publishing the progress....
	                    if (fileLength > 0) // only if total length is known
	                        publishProgress((int) (total * 100 / fileLength));
	                    output.write(data, 0, count);
	                }
	            } catch (Exception e) {
	                return e.toString();
	            } finally {
	                try {
	                    if (output != null)
	                        output.close();
	                    if (input != null)
	                        input.close();
	                } 
	                catch (IOException ignored) { }

	                if (connection != null)
	                    connection.disconnect();
	            }
	        } finally {
	            wl.release();
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        mProgressDialog.show();
	    }

	    @Override
	    protected void onProgressUpdate(Integer... progress) {
	        super.onProgressUpdate(progress);
	        // if we get here, length is known, now set indeterminate to false
	        mProgressDialog.setIndeterminate(true);
	        mProgressDialog.setMax(100);
	        mProgressDialog.setProgress(progress[0]);
	    }

	    @Override
	    protected void onPostExecute(String result) {
	        mProgressDialog.dismiss();
	        if (result != null)
	            Toast.makeText(context,"Sorry, Download error", Toast.LENGTH_LONG).show();
	        else
	            Toast.makeText(context,"Save picture already!", Toast.LENGTH_SHORT).show();
	    }
	}

}

