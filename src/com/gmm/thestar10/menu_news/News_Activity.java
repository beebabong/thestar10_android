package com.gmm.thestar10.menu_news;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gmm.thestar10.R;
import com.gmm.thestar10.menu_Adapter;
import com.gmm.thestar10.API.BannerAPI;
import com.gmm.thestar10.API.NewsAPI;
import com.gmm.thestar10.ApplicationConfig.BannerAdapter;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.gmm.thestar10.flyoutmenu.FlyOutContainer;
import com.gmm.thestar10.menu_gallery.Gallery_Activity;
import com.gmm.thestar10.menu_setting.Setting_Activity;
import com.gmm.thestar10.menu_video.Video_Activity;
import com.gmm.thestar10.object.Banner;
import com.gmm.thestar10.object.BannerGroup;
import com.gmm.thestar10.object.News;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class News_Activity extends Activity implements OnClickListener,OnTouchListener,Runnable {
	
	// menu
		FlyOutContainer root;
		ListView listview_main_menu;
		ImageView btn_menu;
		
	//Activity in this
		LinearLayout layout_shownews;
		ViewFlipper viewflipBanner;
		ScrollView scrollview_layout;
		LinearLayout mainlayout;
		
		
	//Other
	    DisplayImageOptions options;
		protected ImageLoader imageLoader = ImageLoader.getInstance();
		GestureDetector gestureDetector;
		Typeface type;
		 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		  
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		
		this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.ac_news, null);
		this.setContentView(root);
		
		 options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.img_profile)
			.showImageForEmptyUri(R.drawable.img_profile)
			.showImageOnFail(R.drawable.img_profile).cacheInMemory()
			.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
	        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

	        type=Typeface.createFromAsset(getAssets(),"THSarabun_Bold.ttf");
		setmenu();
		mapping();
		setAction();

		new Thread(this).start();
		super.onResume();
	}
	
	private void setAction() {
//		layout_shownews.setOnTouchListener(this);
//		scrollview_layout.setOnTouchListener(this);
	}
	
	private void mapping() {
		
		layout_shownews = (LinearLayout) root.findViewById(R.id.layout_shownews);
		viewflipBanner = (ViewFlipper) root.findViewById(R.id.viewflipBanner);
		scrollview_layout = (ScrollView) root.findViewById(R.id.scrollview_layout);
		mainlayout = (LinearLayout) root.findViewById(R.id.mainlayout); 
	}


	//******************************************************************************************************************************************************
	//****************************************************************SET RUNNABLE**************************************************************************
	//******************************************************************************************************************************************************
	
	@Override
	public void run() {																								// RUN
		if(Unsteady.List_BannerGroup.size()==0) Unsteady.List_BannerGroup = new BannerAPI().getJSONFromUrl();
		if(Unsteady.news.size()==0) Unsteady.news = new NewsAPI().getJSONFromUrl();
		
		handler.sendEmptyMessage(0);	
		
	}
	
	 private Handler handler = new Handler() {																		// HANDLER
		    @Override
		    
		    public void handleMessage(Message msg) {

		    	new DownloadImageTask().execute();
		    	Log.i("banner"+Unsteady.List_BannerGroup.size()+"//"+Unsteady.bannerBitmap.size()+"//"+Unsteady.news.size());
				
		    	for(int i = 0; i < Unsteady.news.size(); i++){
		    		Log.i("news list"+i);
					
    				LayoutInflater inflater = getLayoutInflater();
    				
    				View row = inflater.inflate(R.layout.list_news, null);
    				WrapperShowList wrapper = new WrapperShowList(row);								
    				
    				wrapper.init(Unsteady.news.get(i), i);
    				layout_shownews.addView(row);
		    	}
		    }
	 };
	
	 
	//******************************************************************************************************************************************************
	//************************************************************ WrapperShowList *************************************************************************
	//******************************************************************************************************************************************************
		
	 class WrapperShowList{
			private View row = null;
			private TextView txtHeadline = null;
			private TextView txtdate = null;
			private ImageView imgThumb = null;
			private LinearLayout layout_list = null;
			
			
			public WrapperShowList(View row){
				this.row = row;
			}
			
			@SuppressLint("ResourceAsColor")
			private void init(final News news,final int pos){
				
				getTxtHeadline().setTypeface(type);
				getTxtdate().setTypeface(type);
				
				getTxtHeadline().setText(news.getCOL_title());
				getTxtHeadline().setTextColor(Color.BLACK);
				getTxtdate().setText(news.getCOL_releaseDate());

				LinearLayout.LayoutParams flp= new LinearLayout.LayoutParams(180, 125);
				flp.setMargins(2, 2, 2, 2);
				getImgThumb().setLayoutParams(flp);
				
				imageLoader.displayImage(news.getCOL_thmImg(), getImgThumb(), options);
				getLayout_list().setOnClickListener(new OnClickListener() {
					
					@SuppressWarnings("deprecation")
					@Override
					public void onClick(View v) {
						getLayout_list().setBackgroundColor(Color.LTGRAY);
						
						insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("InNews_Activity",new Intent(News_Activity.this,InNews_Activity.class)
						.putExtra("position", pos)
						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
						.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
					}
				});
				
				
			}
			private LinearLayout getLayout_list(){
				if(layout_list == null){
					layout_list = (LinearLayout)row.findViewById(R.id.layout_list);
				}
				return layout_list;
			}
			private TextView getTxtHeadline(){
				if(txtHeadline == null){
					txtHeadline = (TextView)row.findViewById(R.id.txt_headline);
				}
				return txtHeadline;
			}
			private TextView getTxtdate(){
				if(txtdate == null){
					txtdate = (TextView)row.findViewById(R.id.txt_date);
				}
				return txtdate;
			}
			private ImageView getImgThumb(){																		// SIGN OF DW IN SONG
				if(imgThumb == null){
					imgThumb = (ImageView)row.findViewById(R.id.img_thumb);
				}
				return imgThumb;
			}
			
	 }
	 
	 
	//******************************************************************************************************************************************************
	//********************************************************************SET MENU**************************************************************************
	//******************************************************************************************************************************************************
	
	private void setmenu() {
		listview_main_menu = (ListView) root.findViewById(R.id.listview_main_menu);
		btn_menu = (ImageView) root.findViewById(R.id.btn_menu);
		
		listview_main_menu.setAdapter(new menu_Adapter(this,0));

		btn_menu.setOnClickListener(this);
		listview_main_menu.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long arg3) {
				Log.i("click :"+pos);
				 
				String ActivityTarget = null;
		        Class<?> classTarget = null;
		        
		        switch (pos) {
				case 1: ActivityTarget="Video_Activity"; classTarget = Video_Activity.class; break;
				case 2: ActivityTarget="Gallery_Activity"; classTarget = Gallery_Activity.class; break;
		        case 3: ActivityTarget="Setting_Activity"; classTarget = Setting_Activity.class; break;
		        
		        }
				 
		        insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity(ActivityTarget,new Intent(News_Activity.this,classTarget).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
				.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
				
			}
		});
	}

	private void insertDecorView(View view) {
		getParent().setContentView(
				view,
				new LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
	}
	
	//******************************************************************************************************************************************************
	//********************************************************************SET BANNER************************************************************************
	//******************************************************************************************************************************************************
			
	
	public class DownloadImageTask extends AsyncTask<String, Void, Void> {
		protected Void doInBackground(String... urls) {
			try {
				if(Unsteady.bannerBitmap.size()==0){
					Unsteady.bannerBitmap = new ArrayList<Bitmap>();
					if (Unsteady.List_BannerGroup.size() != 0) {
						for (BannerGroup item : Unsteady.List_BannerGroup) {
							for(Banner bann : item.getCOL_banner()){
								URL url = new URL(bann.getCOL_imgURL());
								
						        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
						        connection.setDoInput(true);
						        connection.connect();
						        InputStream input = connection.getInputStream();
						        Bitmap myBitmap = BitmapFactory.decodeStream(input);

								Unsteady.bannerBitmap.add(myBitmap);
							}
							
						}
					}else{
						Log.i("Banner : DownloadImageTask == Null");
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (Unsteady.bannerBitmap.size() != 0) {
				new BannerAdapter(News_Activity.this, viewflipBanner);
			}
		}
	}
	
	//******************************************************************************************************************************************************
	//************************************************************ SET CLICK / TOUCH ***********************************************************************
	//******************************************************************************************************************************************************
		
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_menu:
			root. toggleMenu();
			break;
		}
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		int MIN_DISTANCE = 200;
		float downX = 0, downY = 0, upX, upY;
		
		 switch(event.getAction()){
	        case MotionEvent.ACTION_DOWN: {
	            downX = event.getX();
	            downY = event.getY();
	            return true;
	        }
	        case MotionEvent.ACTION_UP: {
	            upX = event.getX();
	            upY = event.getY();

	            float deltaX = downX - upX;
	            float deltaY = downY - upY;

	            // swipe horizontal?
	            if(Math.abs(deltaX) > MIN_DISTANCE){
	                // left or right
	                if(deltaX < 0 || deltaX > 0) { root. toggleMenu(); }
	            }
	           
	            return true;
	        }
	    }
	    return false;
	}
	
	
}
