package com.gmm.thestar10.Untilitys;

public class Log {
	
	private static String TAG = "The star 10";
	
	public static void i(String s){
		android.util.Log.i(TAG, s);
	}
	
	public static void w(String s){
		android.util.Log.w(TAG, s);
	}
	
	public static void d(String s){
		android.util.Log.d(TAG, s);
	}
	
	public static void e(String s){
		android.util.Log.e(TAG, s);
	}
	
	public static void v(String s){
		android.util.Log.v(TAG, s);
	}
}
