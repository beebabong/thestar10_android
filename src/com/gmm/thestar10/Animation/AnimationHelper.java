package com.gmm.thestar10.Animation;

import android.content.Context;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class AnimationHelper {

	
	public static Animation inFromRightAnimation(int duration) {

    	Animation inFromRight = new TranslateAnimation(
    													Animation.RELATIVE_TO_PARENT,  
    													+1.0f, 
    													Animation.RELATIVE_TO_PARENT,  
    													0.0f,
    													Animation.RELATIVE_TO_PARENT,  
    													0.0f, 
    													Animation.RELATIVE_TO_PARENT,   
    													0.0f
    													);
    	inFromRight.setDuration(duration);
    	inFromRight.setInterpolator(new AccelerateInterpolator());
    	return inFromRight;
	}
	
    public static Animation outToLeftAnimation(int duration) {
    	Animation outtoLeft = new TranslateAnimation(
    												Animation.RELATIVE_TO_PARENT,  
    												0.0f, 
    												Animation.RELATIVE_TO_PARENT,  
    												-1.0f,
    												Animation.RELATIVE_TO_PARENT,  
    												0.0f, 
    												Animation.RELATIVE_TO_PARENT,   
    												0.0f
    												);
    	outtoLeft.setDuration(duration);
    	outtoLeft.setInterpolator(new AccelerateInterpolator());
    	return outtoLeft;
    }
    
    // for the next movement
    public static Animation inFromLeftAnimation(int duration) {
    	Animation inFromLeft = new TranslateAnimation(
    												Animation.RELATIVE_TO_PARENT,  
    												-1.0f, 
    												Animation.RELATIVE_TO_PARENT,  
    												0.0f,
    												Animation.RELATIVE_TO_PARENT,  
    												0.0f, 
    												Animation.RELATIVE_TO_PARENT,   
    												0.0f
    												);
    	inFromLeft.setDuration(duration);
    	inFromLeft.setInterpolator(new AccelerateInterpolator());
    	return inFromLeft;
    }
    
    public static Animation outToRightAnimation(int duration) {
    	Animation outtoRight = new TranslateAnimation(
    												Animation.RELATIVE_TO_PARENT,  
    												0.0f, 
    												Animation.RELATIVE_TO_PARENT,  
    												+1.0f,
    												Animation.RELATIVE_TO_PARENT,  
    												0.0f, 
    												Animation.RELATIVE_TO_PARENT,   
    												0.0f
    												);
    	outtoRight.setDuration(duration);
    	outtoRight.setInterpolator(new AccelerateInterpolator());
    	return outtoRight;
    }
    
    public static Animation RotateAnimation(Context context, int duration) {

    	AnimationSet set = new AnimationSet(true);

    	RotateAnimation ra = new RotateAnimation(
    											0, 
    											duration,
		    									Animation.RELATIVE_TO_SELF, 
		    									0.5f, 
		    									Animation.RELATIVE_TO_SELF,
		    									0.5f
		    									);

    	ra.setDuration(duration);

    	set.addAnimation(ra);

    	return set;
    }
    
	  public static Animation setLayoutAnim_slideupfrombottom(ViewGroup panel, Context ctx) {
		  AnimationSet set = new AnimationSet(true);
		  Animation animation = new AlphaAnimation(0.0f, 1.0f);
		  animation.setDuration(100);
		  set.addAnimation(animation);
		  animation = new TranslateAnimation(
				  							Animation.RELATIVE_TO_SELF, 
				  							0.0f, 
				  							Animation.RELATIVE_TO_SELF, 
				  							0.0f,
				  							Animation.RELATIVE_TO_SELF, 
				  							1.0f, 
				  							Animation.RELATIVE_TO_SELF, 
				  							0.0f
				  							);
		    animation.setDuration(1000);
		    set.addAnimation(animation);

		    LayoutAnimationController controller =
		        new LayoutAnimationController(set, 0.25f);
		    panel.setLayoutAnimation(controller);
		    
		    return set;
	  }
	  
	  public static Animation setLayoutAnim_slidedownfromtop(ViewGroup panel, Context ctx) {
		  AnimationSet set = new AnimationSet(true);
		  Animation animation = new AlphaAnimation(0.0f, 1.0f);
		  animation.setDuration(100);
		  set.addAnimation(animation);
		  animation = new TranslateAnimation(
				  							Animation.RELATIVE_TO_SELF, 
				  							0.0f, 
				  							Animation.RELATIVE_TO_SELF, 
				  							0.0f,
				  							Animation.RELATIVE_TO_SELF, 
				  							-1.0f, 
				  							Animation.RELATIVE_TO_SELF, 
				  							0.0f
				  							);
		    animation.setDuration(500);
		    set.addAnimation(animation);

		    LayoutAnimationController controller =
		        new LayoutAnimationController(set, 0.25f);
		    panel.setLayoutAnimation(controller);
		    
		    return set;
		  }
	  
	  public static Animation rotateAndScale(Context c , long duration) {
		    AnimationSet set = new AnimationSet(false);

		    ScaleAnimation sa = new ScaleAnimation(
		    									0.0f, 
		    									1.0f, 
		    									0.0f, 
		    									1.0f,
		    									Animation.RELATIVE_TO_SELF, 
		    									0.5f, 
		    									Animation.RELATIVE_TO_SELF,
		    									0.5f
		    									);
		    
		    
		    
		    sa.setDuration(1000);
		    //sa.setRepeatCount(5);
		    //sa.setRepeatMode(Animation.REVERSE);
		    

	    	RotateAnimation ra = new RotateAnimation(
	    											0, 
	    											duration,
			    									Animation.RELATIVE_TO_SELF, 
			    									0.5f, 
			    									Animation.RELATIVE_TO_SELF,
			    									0.5f
			    									);
	    	ra.setDuration(duration);

	    	set.addAnimation(ra);

		    set.addAnimation(sa);
		    //set.setInterpolator(new OvershootInterpolator(1.5f));

		    return set;
		  }
}
