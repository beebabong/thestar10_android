package com.gmm.thestar10.API;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;

import android.os.StrictMode;

import com.gmm.thestar10.APIParse.GalleryParseJson;
import com.gmm.thestar10.Untilitys.Constants;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.object.Gallerys;
 
public class GalleryAPI {
 
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    GalleryParseJson Jgal = new GalleryParseJson();
    // constructor
    public GalleryAPI() {
 
    }
 
    public  List<Gallerys> getJSONFromUrl() {
 
    	BufferedReader in = null;
		String result = null;
		
    	if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
    	
//		String url = "http://thestar9.gmmwireless.com:80/thestar9/star10/api//gallery.jsp?APP_ID=283&DEVICE=mockup_test&CHARSET=utf-8&MSISDN=66818667415&APPVERSION=1.0&APIVERSION=2.0.0";
		 
		    
		try {
			String url = "http://thestar9.gmmwireless.com:80/thestar9/star10/api//gallery.jsp?"
					+"APP_ID="+ Constants.APP_ID 
					+ "&DEVICE=" + Constants.DEVICE
					+ "&CHARSET=" + Constants.CHARSET 
					+ "&MSISDN=" + Constants.MSISDN 
					+ "&APPVERSION=" + Constants.APP_VERSION
					+ "&APIVERSION=" + Constants.API_VERSION;
	
			Log.i("url gallery : "+url);
	
			HttpClient client = new DefaultHttpClient();
	
			HttpConnectionParams.setConnectionTimeout(client.getParams(),Constants.TIMEOUT_CONNECTION);
			HttpConnectionParams.setSoTimeout(client.getParams(),Constants.TIMEOUT_SOCKET);
	
			HttpGet request = new HttpGet(url);
			request.setHeader("User-Agent", "Android " + android.os.Build.VERSION.RELEASE + "/" + android.os.Build.MODEL);
	
			HttpResponse response = client.execute(request);
	
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
	
			result = sb.toString();
			
			Log.i("result gallery : "+result);
		}catch(Exception e){
		
	
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return Jgal.setvalueInclass(result);
    }
}