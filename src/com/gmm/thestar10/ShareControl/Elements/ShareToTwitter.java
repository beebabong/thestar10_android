package com.gmm.thestar10.ShareControl.Elements;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;

import com.gmm.thestar10.Untilitys.Log;

public class ShareToTwitter {
String url,word;
Context mContext;
ResolveInfo info;
	public ShareToTwitter(Context mContext,ResolveInfo info, String word, String url) {
			this.url = url;
			this.word = word;
			this.mContext = mContext;
			this.info = info;
			this.shareTwitter();
		
	}

	private void shareTwitter() {

		Log.i("-- shareTwitter");

		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setClassName(info.activityInfo.packageName,info.activityInfo.name);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, "AIS Music Store");
		intent.putExtra(Intent.EXTRA_TEXT, "Recommend  " + word
				+ " Listen to it now. " + url);
		((Activity) mContext).startActivity(intent);
	}
}
