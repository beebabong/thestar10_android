package com.gmm.thestar10.ShareControl.Elements;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;

import com.gmm.thestar10.Untilitys.Log;

public class ShareToGmail {
	String url, word;
	Context mContext;
	ResolveInfo info;

	public ShareToGmail(Context mContext, ResolveInfo info, String word,
			String url) {
		this.url = url;
		this.word = word;
		this.mContext = mContext;
		this.info = info;
		this.shareGmail();

	}

	private void shareGmail() {
		Log.i("-- shareGmail");

		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setClassName(info.activityInfo.packageName,
				info.activityInfo.name);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, "I want to share " + word
				+ " from AIS Music Store with you!");
		intent.putExtra(
				Intent.EXTRA_TEXT,
				"Hello,"
						+ "\n"
						+ "\n"
						+ "\n"
						+ "I want to share "
						+ word
						+ " Click the link "
						+ "below to listen to "
						+ "it now on AIS Music Store: "
						+ "\n"
						+ url
						+ "\n"
						+ "\n"
						+ "Enjoy! unlimited listen to your favorite music anywhere, anytime!"
						+ "\n" + "\n" + "See you soon at AIS Music Store.");
		((Activity) mContext).startActivity(intent);
	}

}
