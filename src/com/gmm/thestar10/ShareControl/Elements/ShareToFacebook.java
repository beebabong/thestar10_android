package com.gmm.thestar10.ShareControl.Elements;

import android.content.Context;
import android.os.Bundle;

import com.gmm.thestar10.R;
import com.gmm.thestar10.Untilitys.Constants;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.facebook.android.DialogError;
import com.gmm.thestar10.facebook.android.Facebook;
import com.gmm.thestar10.facebook.android.Facebook.DialogListener;
import com.gmm.thestar10.facebook.android.FacebookError;
import com.gmm.thestar10.facebook.android.FacebookHelper;

public class ShareToFacebook {
	String url, cover, word;
	Context context;

	public ShareToFacebook(Context context, String word, String cover,
			String url) {
		this.url = url;
		this.word = word;
		this.cover = cover;
		this.context = context;
		shareFacebookGOTOMusicStore();

	}

	public void shareFacebookGOTOMusicStore() {
		Log.i("-- shareFacebook");

		FacebookHelper.mFacebook = new Facebook(Constants.APP_FACEBOOK_ID);

		Bundle params = new Bundle();
		params.putString("link", url);
		params.putString("caption", "http://musicstore.ais.co.th/");
		params.putString("description", "");
		params.putString("message", "Message");
		params.putString("redirect_uri", "http://musicstore.ais.co.th/");
		params.putString(
				"action",
				"[{\"name\"" + word + " "
						+ context.getString(R.string.app_name) + "\", \"link\""
						+ url + "\"}]");
		params.putString("picture", cover);
		params.putString("name", "Recommend " + word
				+ " Listen to it now on AIS Music Store " + url);

		FacebookHelper.mFacebook.dialog(context, "feed", params,
				new DialogListener() {
					public void onFacebookError(FacebookError e) {
					}

					public void onError(DialogError e) {
					}

					public void onComplete(Bundle values) {
						
//						Toast.makeText(context, "Post Complete",
//								Toast.LENGTH_SHORT).show();
					}

					public void onCancel() {
					}
				});

	}
}
