package com.gmm.thestar10.ShareControl.Elements;

import android.content.Context;
import android.content.Intent;

import com.gmm.thestar10.Untilitys.Log;

public class ShareToSMS {
	String url, word;
	Context mContext;

	public ShareToSMS(Context mContext, String word, String url) {
		this.url = url;
		this.word = word;
		this.mContext = mContext;
		this.shareMMS();

	}

	// ShareMMS
	private void shareMMS() {
		Log.i("-- shareMMS");

		Intent newIntent = new Intent(Intent.ACTION_VIEW);
		newIntent.putExtra("address", /* phoneNumber */""); // Default Address to
		newIntent.putExtra("sms_body", "Listen to " + word
				+ " on AIS Music Store: " + url); // Default
													// Message
		newIntent.setType("vnd.android-dir/mms-sms");
		mContext.startActivity(newIntent);

	}

}
