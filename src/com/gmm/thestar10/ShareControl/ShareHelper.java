package com.gmm.thestar10.ShareControl;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;

import com.gmm.thestar10.R;
import com.gmm.thestar10.ShareControl.Elements.ShareOther;
import com.gmm.thestar10.ShareControl.Elements.ShareToEmail;
import com.gmm.thestar10.ShareControl.Elements.ShareToFacebook;
import com.gmm.thestar10.ShareControl.Elements.ShareToGmail;
import com.gmm.thestar10.ShareControl.Elements.ShareToSMS;
import com.gmm.thestar10.ShareControl.Elements.ShareToTwitter;

public class ShareHelper {

	public static final byte SHARE_DEFAULT = 0; 
	public static final byte SHARE_BY_CONTEXT = 1;
		private Context mContext;
		private String word,cover,url;
		
		public ShareHelper(Context context,String word,String cover ,String url) {
			this.mContext = context;
			this.cover = cover;
			this.url = url;
			share();
		}

		public void share() {
			Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
			sendIntent.setType("text/plain");
			List activities = mContext.getPackageManager().queryIntentActivities(sendIntent, 0);
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle("Share");
			
			final ShareIntentListAdapter adapter = new ShareIntentListAdapter((Activity) mContext, R.layout.row_share_intent,
					activities.toArray());
			
			
			builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					ResolveInfo info = (ResolveInfo) adapter.getItem(which);
					String packageName = info.activityInfo.packageName;
					if (packageName.contains("facebook")) {
						new ShareToFacebook(mContext, word,cover,url);
					} else if (packageName.contains("mms")) {
						new ShareToSMS(mContext, word,url);
					} else if (packageName.contains("twitter")) {
						new ShareToTwitter(mContext, info, word,url);
					} else if (packageName.contains("com.google.android.gm")) {
						new ShareToGmail(mContext, info, word,url);
					} else if (packageName.contains("com.android.email")) {
						new ShareToEmail(mContext, info, word,url);
					} else {
						new ShareOther(mContext, info,word,url);
					}
				}
			});
			
			
			builder.create().show();
			
		}
}




/****** WORDING
 * PLAYLIST[ME]		==> ชื่อ Playlist Created by ชื่อเจ้าของ Playlist
 * PLAYLIST[FRIEND]	==>	ชื่อ Playlist ของเพื่อน Created by ชื่อเพื่อน	
 * ALBUM    		==> ชื่ออัลบั้ม by ชื่อศิลปิน
 * ARTIST			==> Artist Name 
 * SONG				==> ชื่อเพลง by ชื่อศิลปิน 
 */
	
	
