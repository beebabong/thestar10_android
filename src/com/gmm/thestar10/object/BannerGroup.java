package com.gmm.thestar10.object;

import java.util.List;

public class BannerGroup {
	 String COL_groupName;
	 List<Banner> COL_banner;
	
	
	 public String getCOL_groupName() {
		return COL_groupName;
	}
	public void setCOL_groupName(String cOL_groupName) {
		COL_groupName = cOL_groupName;
	}
	public List<Banner> getCOL_banner() {
		return COL_banner;
	}
	public void setCOL_banner(List<Banner> cOL_banner) {
		COL_banner = cOL_banner;
	}
	
	 
}
