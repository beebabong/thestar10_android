package com.gmm.thestar10.object;

import java.util.List;

public class VideoHightlight {
	 String COL_googleVCode;
	 String COL_thumbsImg;
	 String COL_headline;
	 String COL_caption;
	 String COL_desc;
	 String COL_releaseDate;
	 String COL_lastModifiedDate ;	
	 List<VideoPart> COL_videoParts;
	 
	String COL_videoID;
	 public String getCOL_videoID() {
		return COL_videoID;
	}
	public void setCOL_videoID(String cOL_videoID) {
		COL_videoID = cOL_videoID;
	}
	public String getCOL_googleVCode() {
		return COL_googleVCode;
	}
	public void setCOL_googleVCode(String cOL_googleVCode) {
		COL_googleVCode = cOL_googleVCode;
	}
	public String getCOL_thumbsImg() {
		return COL_thumbsImg;
	}
	public void setCOL_thumbsImg(String cOL_thumbsImg) {
		COL_thumbsImg = cOL_thumbsImg;
	}
	public String getCOL_headline() {
		return COL_headline;
	}
	public void setCOL_headline(String cOL_headline) {
		COL_headline = cOL_headline;
	}
	public String getCOL_caption() {
		return COL_caption;
	}
	public void setCOL_caption(String cOL_caption) {
		COL_caption = cOL_caption;
	}
	public String getCOL_desc() {
		return COL_desc;
	}
	public void setCOL_desc(String cOL_desc) {
		COL_desc = cOL_desc;
	}
	public String getCOL_releaseDate() {
		return COL_releaseDate;
	}
	public void setCOL_releaseDate(String cOL_releaseDate) {
		COL_releaseDate = cOL_releaseDate;
	}
	public String getCOL_lastModifiedDate() {
		return COL_lastModifiedDate;
	}
	public void setCOL_lastModifiedDate(String cOL_lastModifiedDate) {
		COL_lastModifiedDate = cOL_lastModifiedDate;
	}
	public List<VideoPart> getCOL_videoParts() {
		return COL_videoParts;
	}
	public void setCOL_videoParts(List<VideoPart> cOL_videoParts) {
		COL_videoParts = cOL_videoParts;
	}
	
}
