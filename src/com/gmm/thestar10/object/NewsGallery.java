package com.gmm.thestar10.object;

public class NewsGallery {

	 String COL_ID ;
	 String COL_newsGalleryImgURL;
	 
	 	public String getCOL_ID() {
			return COL_ID;
		}
		public void setCOL_ID(String cOL_ID) {
			COL_ID = cOL_ID;
		}
		public String getCOL_newsGalleryImgURL() {
			return COL_newsGalleryImgURL;
		}
		public void setCOL_newsGalleryImgURL(String cOL_newsGalleryImgURL) {
			COL_newsGalleryImgURL = cOL_newsGalleryImgURL;
		}
}
