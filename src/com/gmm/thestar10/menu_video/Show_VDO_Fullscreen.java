package com.gmm.thestar10.menu_video;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebSettings.PluginState;

import com.gmm.thestar10.R;

public class Show_VDO_Fullscreen extends Activity {
	WebView mWebView;
	String googleVCodePart;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		
		googleVCodePart = getIntent().getStringExtra("VCodePart");
		
		
		mWebView = (WebView) findViewById(R.id.webView1);
		
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setPluginState(PluginState.ON);
		mWebView.loadUrl("http://www.youtube.com/embed/" + googleVCodePart + "?autoplay=1&vq=small");
		mWebView.setWebChromeClient(new WebChromeClient());
	}
	
	
}
